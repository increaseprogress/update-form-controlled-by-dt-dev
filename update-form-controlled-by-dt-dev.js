

colesLiquor.updateForm = function ($) {
  
    // Add class to allow for greater specificifty with css styling
    $('#mobilesignupform').addClass('updateYourPreferences');
//    $('#mobilesignupform #preferences fieldset h4:first').remove();
    
    var getParameterByName = function (data, name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(data);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };

    var init = function () {



        //example 1: http://1creplatform.local/updatesignup?name=Raphael+Villareal&email=raphael.villareal%40gmail.com&dob=1990-10-19&postcode=3181

        var inputString = location.search;
        var decodedString = decodeURIComponent(inputString.replace(/\+/g, '%20'));
        var name = getParameterByName(decodedString, "name");
        var email = getParameterByName(decodedString, "email");
        var postcode = getParameterByName(decodedString, "postcode");
        var dob = getParameterByName(decodedString, "dob");

        // init ezMark custom checkbox for desktop only
        if(!$('body').hasClass('is-mobile')) {
            $('.email-prefs input[type="checkbox"], input#calls, input#sms').ezMark();
        }

        $('input#calls').closest('label').addClass('checkBoxCall');
        $('input#sms').closest('label').addClass('checkBoxSms');

        $('.checkBoxSms').closest('div').addClass('mobileContact');
        $('.email-prefs').after('<img class="exclusive" src="/Assets/images/dataacquisition/exclusive.png">');
        $('#mobilesignupform input[name=flybuys]').after('<p class="note">Find the 16 digit number on the back of your card.</p>');

        if (name == '' || email == '' || postcode == '' || dob == '')
            return;
        
        var dobParts = dob.split("-");

        // console.log("Name: " + name);
        // console.log("DOB: " + dob);
        // console.log("Postcode: " + postcode);
        // console.log("Email: " + email);

        $("#mobilesignupform input[name='email']").val(email);
        $("#mobilesignupform input[name='firstname']").val(name);
        $("#mobilesignupform input[name='postcode']").val(postcode);
        $("#mobilesignupform input[name='postcode']").val(postcode);

        $("#mobilesignupform select[name$='dropDay']").val(dobParts[2]);
        $("#mobilesignupform select[name$='dropMonth']").val(dobParts[1]);
        $("#mobilesignupform select[name$='dropYear']").val(dobParts[0]);

        $("#mobilesignupform #preferencesbutton").on('click', function() {
            setTimeout(function () {
                if($('#thankyou').is(':visible')) {
                    $('html,body').scrollTop(0);
                }
            },500);
            
        });

        // Groups elements of mobile.right
        $('#mobilesignupform #preferences fieldset h4:first, #mobilesignupform #preferences fieldset p:first, #mobilesignupform #preferences fieldset .mobileContact').wrapAll('<div class="block-mobile right"/>');
    
        
         // Adds parent div to group mobile block left & right
        $('#mobilesignupform #preferences fieldset .exclusive, #preferences fieldset .block-mobile.right').wrapAll('<div class="block-update-details block-mobile"/>');
      
        // Adds class to exclusive image
        $('#mobilesignupform #preferences fieldset .exclusive').addClass('block-mobile left');
    
        // Groups elementst of flybys-right
        $('.block-update-details.block-mobile').nextUntil('#preferencesbutton').wrapAll('<div class="block-flybuys right"/>');
    
        // Adds flybys image
        $('.block-flybuys.right').before('<div class="block-flybuys left"><img title="Flybuys" alt="Flybuys Logo" src="/~/media/Images/dataacquisition/flybuys.ashx"/></div>');
    
         // Adds parent div to group mobile block left & right
        $('#mobilesignupform #preferences fieldset .block-flybuys.left, #preferences fieldset .block-flybuys.right').wrapAll('<div class="block-update-details block-flybuys"/>');
    
        
        // Adds class to label element based on 'for' attribute for easier css styling
        $("label").each(function() {
            var src = $(this).attr('for');
            var a = $(this).addClass(src);
        });
        // Adds class to input element based on 'name' attribute for easier css styling
        $("input").each(function() {
            var src = $(this).attr('name');
            var a = $(this).addClass(src);
        });
        
        
        // Reposition note text
        $(".is-mobile .block-flybuys .note").insertAfter('.block-flybuys label.flybuys');

        setTimeout(function () {
            $("#mobilesignupform #signupbutton").click();
            setTimeout(function () {
                $("#mobilesignupform #preferences h1+p").remove(); //removes "You are now subscribed"
                $("#updateFormPleaseWait").remove(); //removes the "please wait" div
                $("#mobilesignupform").css("display", "block");
            }, 250);
        }, 250);
    };

    return {
        init: init
    };
} (jQuery);

$(document).ready(function () {
    colesLiquor.updateForm.init();
});

$(window).load(function() {
    $('.is-mobile .exclusive').remove();
  
    // CSS STYLING
    $('h1').css({
      'font-size':'34px',
      'padding-top':'25px'
    });
    $('.updateSignup #mobilesignupform').css({
      'width':'100%'
    });
    $('.updateYourPreferences .block-mobile.right').css({
      'width':'668px',
      'display':'inline-block'
    });
    $('.updateSignup #mobilesignupform input').css({
      'padding':'6px 12px',
      'border':'1px solid #c1c1c1',
      'font-size':'17px'
    });
    $('.updateYourPreferences .email-prefs').css({
      'background':'#252d6c',
      'max-width':'100%',
      'width':'794px',
      'height':'auto',
      'padding':'32px',
      'margin':'30px 0 60px 0' 
    });
    $('.updateSignup #mobilesignupform .email-prefs label').css({
        'width':'auto',
        'margin-right':'26px',
        'font-size':'17px',
        'font-weight':'normal'
    });
    $('.updateSignup #mobilesignupform .email-prefs label:last').css({
      'margin-right':'0' 
    });
    $('.updateSignup .ez-checkbox').css({
      'width':'36px' 
    });
    $('.updateYourPreferences .block-update-details').css({
      'margin-top':'50px',
      'padding-top':'50px',
      'border-top':'1px solid #c1c1c1'
    });    
    $('.updateYourPreferences .block-update-details:first').css({
      'border':'none',
      'margin-top':'0',
      'padding-top':'0'  
    });    
    $('.updateYourPreferences .block-update-details:last').css({
      'border-bottom':'1px solid #c1c1c1',
      'margin-bottom':'32px',
      'padding-bottom':'50px'
    });    
    $('.updateYourPreferences .block-update-details .left').css({
      'width':'124px',
      'display':'inline-block',
      'margin-right':'22px',
      'vertical-align':'top'
      
    });    
    $('.updateYourPreferences .block-update-details .left img').css({
      'width':'100%',
      'height':'auto'
    });    
    $('.updateYourPreferences .block-update-details h4').css({
      'border':'none',
      'margin':'0 0 14px 0',
      'padding-top':'0'  
    });
    $('.updateYourPreferences .block-update-details label').css({
      'font-size':'17px',
      'display':'inline-block',
      'margin':'15px 12px 0 0'
    });
    $('.updateYourPreferences .block-update-details input').css({
      'font-size':'17px',
      'display':'inline-block',
      'max-width':'229px'
    });
    $('.updateYourPreferences .block-update-details .right').css({
      'width':'668px',
      'display':'inline-block'
    });
    $('.updateYourPreferences .block-update-details .right p').css({
      'font-size':'17px',
      'line-height':'1.3'
    });
    $('.updateYourPreferences .block-update-details .right p.note').css({
      'font-size':'11px',
      'max-width':'131px'
    });
    $('.updateYourPreferences .block-mobile .mobileContact label.checkBoxSms, .updateYourPreferences .block-mobile.right .mobileContact label.checkBoxCall').css({
      'width':'auto',
      'margin-top':'26px',
      'font-weight':'normal',
      'line-height':'30px'
    });
    $('.updateYourPreferences .block-flybuys label.checkBoxSms, .updateYourPreferences .block-mobile.right .mobileContact label.checkBoxCall').css({
      'width':'auto',
      'margin-top':'26px',
      'font-weight':'normal',
      'line-height':'30px'
    });
    $('.updateSignup #mobilesignupform input.defaultButton').css({
      'margin-bottom':'32px'
    });

    // REMOVE ELEMENT ON MOBILE
    

    // CSS STYLING - MOBILE 
    $('.is-mobile.updateSignup #mobilesignupform fieldset').width('100%');
    
    $('.is-mobile #mobilesignupform input.defaultButton').css({
      'margin-bottom':'32px'
    });
    $('.is-mobile .email-prefs').css({
      'width':'100%',
      'padding':'12px 18px 22px 18px'
    });
    $('.is-mobile #mobilesignupform .email-prefs label').css({
      'width':'50%',
      'margin-right':'0'
    });
    $('.updateYourPreferences .block-update-details').css({
      'margin-top':'30px',
      'padding-top':'30px',
      'border-top':'1px solid #c1c1c1'
    });    
    
    $('.updateYourPreferences .block-update-details input').css({
      'margin-right':'10px',
      'font-size':'17px',
      '-webkit-border-radius':'2px',
      '-moz-border-radius':'2px',
      'border-radius':'2px'
   });
    $('.updateYourPreferences .block-update-details:last').css({
      'border-bottom':'1px solid #c1c1c1',
      'margin-bottom':'30px',
      'padding-bottom':'40px'
    });    
   
    $('.updateYourPreferences .block-update-details input.mobile').css({
      'width':'100%',
      'max-width':'100%',
      'display':'block'
    });    
    $('.updateYourPreferences .email-prefs').css({
      'margin-bottom':'30px' 
    });
    $('.is-mobile.updateSignup .block-update-details .right').width('100%');
    
    $('.is-mobile.updateSignup .block-update-details .right').css({
      'display':'inline-block'
    });
    $(".is-mobile .block-flybuys .note").css({
      'max-width':'100%',
      'margin-bottom':'0'
    });
    
   $('.is-mobile.updateSignup .block-update-details .left,.is-mobile.updateSignup .block-update-details .right').css({
      'display':'block'
    }); 
    
});